Link_Fix_Absolute_URLs 1.0.x-dev, 2024-xx-xx
--------------------------------------------
#3431683 by Project Update Bot, damienmckenna: Automated Drupal 11 compatibility
  fixes.
#3491414 by damienmckenna: Enable testing with gitlab-ci; fix tests.
#3497335 by damienmckenna: Fix cspell issues in 1.0.x; require cspell and
  composer tests pass, ignore stylelint.


Link_Fix_Absolute_URLs 1.0.3, 2023-02-02
----------------------------------------
#3316033 by DuttonMa, DamienMcKenna, arnested: PHP 8.0 compatibility fix.


Link_Fix_Absolute_URLs 1.0.2, 2020-07-29
----------------------------------------
#3161935 by DamienMcKenna: File paths can be broken.


Link_Fix_Absolute_URLs 1.0.1, 2020-07-01
----------------------------------------
By DamienMcKenna: Minor coding standards changes.
#3155819 by DamienMcKenna: Refactor process() method for duplicate code.
#3155824 by DamienMcKenna: Add test coverage for broken/404 links.


Link_Fix_Absolute_URLs 1.0.0, 2020-06-02
----------------------------------------
#3145204 by DamienMcKenna: hook_entity_presave() can fail if field is empty.
#3138234 by DamienMcKenna: Document how to update existing entities.


Link_Fix_Absolute_URLs 1.0.0-rc1, 2020-06-02
--------------------------------------------
#3137127 by DamienMcKenna: Add test coverage for node system paths.
#3144442 by DamienMcKenna: Add test coverage for node path aliases.
#3144456 by DamienMcKenna: Add test coverage for remote URLs.
#3137829 by DamienMcKenna: Transparently handle HTTP and HTTPS URLs.
#3144470 by DamienMcKenna: Refactor comparison logic so it can be reused.


Link_Fix_Absolute_URLs 1.0.0-beta2, 2020-05-19
----------------------------------------------
By DamienMcKenna: Correct GPL reference in composer.json.
#3137802 by DamienMcKenna: WSOD if getFieldDefinitions() method not supported.
#3137805 by DamienMcKenna: Fix tests on D9.


Link_Fix_Absolute_URLs 1.0.0-beta1, 2020-05-15
----------------------------------------------
#3137125 by DamienMcKenna: Initial functionality.
