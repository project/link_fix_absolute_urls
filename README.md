# Link Fix Absolute URLs

Converts URLs saved to link fields to their internal equivalents, which is much
safer to deal with than having hardcoded URLs everywhere.

No configuration is required or available, simply enable the module and all
future links saved to the site will have absolute URLs to the site's URL
converted to the appropriate internal path.

## Custom uses

The module's primary logic has been written as a service that can be reused.

```
$changed = \Drupal::service('link_fix_absolute_urls.link_processor')
  ->process($entity);
// If the entity values were modified by the service, save the entity.
if ($changed) {
  $entity->save();
}
```

However, because the logic is triggered on entity save, all it takes is saving
the entity to correct existing fields:

```$entity->save();
```

This can be used in a `hook_post_update_NAME()` function to correct existing
content.

## Credits / contact

Written by [Damien McKenna](https://www.drupal.org/u/damienmckenna) with a nod
to a module by [Andy Hebrank](https://www.drupal.org/u/ahebrank) which just
gives a warning instead of fixing the URL.

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the [project issue
queue](https://www.drupal.org/project/issues/link_fix_absolute_urls).
