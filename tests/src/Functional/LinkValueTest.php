<?php

namespace Drupal\Tests\link_fix_absolute_urls\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\TestFileCreationTrait;

/**
 * Confirm the module's functionality works as intended.
 *
 * @group link_fix_absolute_urls
 */
class LinkValueTest extends BrowserTestBase {

  // This provides a way of dynamically generating a test file.
  use TestFileCreationTrait;

  /**
   * The theme to use for this test.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    // This module.
    'link_fix_absolute_urls',

    // A helper that includes configuration use for testing.
    'link_fix_test_helper',
  ];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser([
      'administer site configuration',
      'bypass node access',
    ]);
    $this->drupalLogin($this->user);
  }

  /**
   * Verify that empty link fields don't throw errors.
   */
  public function testEmptyLinkField() {
    // Create a second node with a link to the first node.
    $node = $this->createNode([
      'title' => 'Test node',
    ]);
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Verify that regular remote URLs are not changed.
   */
  public function testRemoteUrlNotChanged() {
    $link_to_test = 'https://www.drupal.org/project/link_fix_absolute_urls';

    // Create a second node with a link to the first node.
    $node = $this->createNode([
      'title' => 'Test node',
      'field_test_link' => [
        [
          'uri' => 'https://www.drupal.org/project/link_fix_absolute_urls',
          'title' => 'The link',
        ],
      ],
    ]);
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    // Confirm the link is on the page.
    $this->assertSession()->linkExists('The link');
    $this->assertSession()->linkByHrefExists('https://www.drupal.org/project/link_fix_absolute_urls');

    // Confirm the URL value is stored correctly and not modified.
    $value = $node->get('field_test_link')->getValue();
    $this->assertEquals($value[0]['uri'], 'https://www.drupal.org/project/link_fix_absolute_urls');
  }


  /**
   * Verify that node paths are changed.
   */
  public function testNodePath() {
    // Create a node.
    $node1 = $this->createNode([
      'title' => 'Test node 1',
    ]);

    // Confirm the nodes can be viewed.
    $this->drupalGet('node/' . $node1->id());
    $this->assertSession()->statusCodeEquals(200);

    // Create a second node with a link to the first node.
    $node2 = $this->createNode([
      'title' => 'Test node 2',
      'field_test_link' => [
        [
          'uri' => $this->baseUrl . '/node/' . $node1->id(),
          'title' => 'The link',
        ],
      ],
    ]);
    $this->drupalGet('node/' . $node2->id());
    $this->assertSession()->statusCodeEquals(200);

    // Confirm the link is on the page. Note that the URL will be output as a
    // relative URL.
    $this->assertSession()->linkExists('The link');
    $this->assertSession()->linkByHrefExists('/node/' . $node1->id());

    // Confirm the URL values.
    $value = $node2->get('field_test_link')->getValue();
    $this->assertEquals($value[0]['uri'], 'entity:node/' . $node1->id());
  }

  /**
   * Verify that node aliases are changed.
   */
  public function testNodePathAlias() {
    // Create a node.
    $node1 = $this->createNode([
      'title' => 'Test node 1',
      'path' => [
        'alias' => '/test-node-1',
      ],
    ]);

    // Confirm the nodes can be viewed.
    $this->drupalGet('node/' . $node1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('test-node-1');
    $this->assertSession()->statusCodeEquals(200);

    // Create a second node with a link to the first node.
    $node2 = $this->createNode([
      'title' => 'Test node 2',
      'path' => [
        'alias' => '/test-node-2',
      ],
      'field_test_link' => [
        [
          'uri' => $this->baseUrl . '/test-node-1',
          'title' => 'The link',
        ],
      ],
    ]);
    $this->drupalGet('node/' . $node2->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('test-node-2');
    $this->assertSession()->statusCodeEquals(200);

    // Confirm the link is on the page. Note that the URL will be output as a
    // relative URL.
    $this->assertSession()->linkExists('The link');
    $this->assertSession()->linkByHrefExists('/test-node-1');

    // Confirm the URL values.
    $value = $node2->get('field_test_link')->getValue();
    $this->assertEquals($value[0]['uri'], 'entity:node/' . $node1->id());
  }

  /**
   * Verify that URLs with HTTP, HTTPS or www. prefixes are handled the same.
   */
  public function testHttpHttpsWww() {
    // Create a node.
    $node1 = $this->createNode([
      'title' => 'Test node 1',
    ]);

    // Confirm the nodes can be viewed.
    $this->drupalGet('node/' . $node1->id());
    $this->assertSession()->statusCodeEquals(200);

    // Try prefixes.
    $prefixes = [
      'http://',
      'https://',
      'http://www.',
      'https://www.',
    ];

    foreach ($prefixes as $ctr => $prefix) {
      $base_url = $this->baseUrl;

      // Remove any HTTP or HTTPS prefix on the base URL.
      $base_url = str_replace('http://', '', $base_url);
      $base_url = str_replace('https://', '', $base_url);

      // Add the current prefix to the base URL.
      $base_url = $prefix . $base_url;

      // Create a second node with a link to the first node.
      $node = $this->createNode([
        'title' => 'Test node ' . $ctr,
        'field_test_link' => [
          [
            'uri' => $base_url . '/node/' . $node1->id(),
            'title' => 'The link',
          ],
        ],
      ]);
      $this->drupalGet('node/' . $node->id());
      $this->assertSession()->statusCodeEquals(200);

      // Confirm the link is on the page. Note that the URL will be output as a
      // relative URL.
      $this->assertSession()->linkExists('The link');
      $this->assertSession()->linkByHrefExists('/node/' . $node1->id());

      // Confirm the URL values.
      $value = $node->get('field_test_link')->getValue();
      $this->assertEquals($value[0]['uri'], 'entity:node/' . $node1->id());
    }
  }

  /**
   * Verify that URLs that don't exist are left as-is.
   */
  public function test404() {
    // Create a node with a link to nowhere.
    $node = $this->createNode([
      'title' => 'Test node',
      'field_test_link' => [
        [
          'uri' => $this->baseUrl . '/this/path/does/not/exist',
          'title' => 'The link',
        ],
      ],
    ]);
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    // Confirm the link is on the page. Note that the URL will be output as a
    // relative URL.
    $this->assertSession()->linkExists('The link');
    $this->assertSession()->linkByHrefExists('/this/path/does/not/exist');
  }

  /**
   * Verify that URLs to local files are handled properly.
   */
  public function testFileLink() {
    // The site's homepage path, need this to get the raw path for the file.
    $base_url = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();

    // Get some test files to work with.
    $files = $this->getTestFiles('image');

    // Only one file is needed.
    $file = $files[0];

    // Get the public external URL for the file.
    $file_path = \Drupal::service('file_url_generator')->generateAbsoluteString($file->uri);

    // The file path without the hostname portion of the URL.
    $file_path_local = str_replace($base_url, base_path(), $file_path);

    // Create a node with a link to nowhere.
    $node = $this->createNode([
      'title' => 'Test node',
      'field_test_link' => [
        [
          'uri' => $file_path,
          'title' => 'The link',
        ],
      ],
    ]);
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    // Confirm the link is on the page.
    $this->assertSession()->linkExists('The link');
    $this->assertSession()->linkByHrefExists($file_path_local);
  }

  /**
   * Verify that system aliases are changed.
   */
  // @code
  // public function testSystemPathAlias() {
  //
  // }
  // @endcode

}
